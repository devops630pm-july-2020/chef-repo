#
# Cookbook:: custom_tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

package 'java-1.8.0-openjdk' do
 action :install
end

package 'java-1.8.0-openjdk-devel' do
 action :install
end

user 'tomcat' do
  comment 'tomcat app User'
  home '/opt/tomcat'
  system true
  shell '/bin/nologin'
end

group 'tomcat' do
  action :create
end

package 'wget' do
 action :install
end

ark 'tomcat' do
  url 'http://apachemirror.wuchna.com/tomcat/tomcat-8/v8.5.57/bin/apache-tomcat-8.5.57.tar.gz'
  home_dir '/opt/tomcat'
  prefix_root '/opt'
  owner 'tomcat'
  version 'apache-tomcat-8.5.57'
end

execute 'GivingPermisions' do
 command 'chmod 755 /opt/tomcat/bin/*.sh'
end

template "/opt/tomcat/conf/tomcat-users.xml" do
  source 'tomcat-users.xml.erb'
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/opt/tomcat/webapps/manager/META-INF/context.xml" do
  source 'manager-context.xml.erb'
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/etc/systemd/system/tomcat.service" do
  source 'tomcat-init.erb'
  owner 'root'
  group 'root'
  mode '0755'
  notifies :restart, 'service[tomcat]', :delayed
end

service 'tomcat' do
  supports :restart => true, :start => true, :stop => true 
  action [ :enable, :start ]
end





